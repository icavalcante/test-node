import NumberExtension from '../../auxiliary/NumberExtension'

describe('Routes Book', () => {
	const path = '/books';
	const Books = app.datasource.models.Books;
	const defaultBook = {
		id: 1,
		name: 'Default Book'
	};
	const newBook = {
		id: 2,
		name: 'new Book'
	};
	beforeEach((done) => {
		Books
		.destroy({ where: {} })
		.then(() => Books.create(defaultBook))
		.then(() => {
			done();
		});
	});

	describe('Route GET /books', () => {
		it('Should return a list of books', (done) => {
			console.log("RESPOSTA DA SOMA",NumberExtension.sum(12.59,0.161));
			console.log("RESPOSTA DA SOMA",NumberExtension.sub(1.59,0.161));
			request
				.get(path)
				.end((err, res) => {
					console.log(res.body);
					expect(res.body[0].id).to.be.eql(defaultBook.id);
					expect(res.body[0].name).to.be.eql(defaultBook.name);
					done(err);
				});
		});
	});

	describe('Route GET /books/{id}', () => {
		it('Should return a book', (done) => {
			request
				.get(`${path}/${defaultBook.id}`)
				.end((err, res) => {
					console.log(res.body);
					expect(res.body.id).to.be.eql(defaultBook.id);
					expect(res.body.name).to.be.eql(defaultBook.name);
					done(err);
				});
		});
	});

	describe('Route POST /books', () => {
		it('Should create a book', (done) => {
			request
				.post(path)
				.send(newBook)
				.end((err, res) => {
					console.log('CREATE RESPONSE', res.body);
					expect(res.body.id).to.be.eql(newBook.id);
					expect(res.body.name).to.be.eql(newBook.name);
					done();
				});
		});
	});

	// describe('Route GET /books/{id}', () => {

	// 	it('Should return BOOK with id 2', done => {
	// 		request
	// 			.get('/books/2')
	// 			.end((err,res) => {
	// 				console.log(res.body);
	// 				expect(res.body.id).to.be.eql(newBook.id);
	// 				expect(res.body.name).to.be.eql(newBook.name);
	// 				done(err);
	// 			});
	// 	});

	// });

	describe('Route PUT /books/{id}', () => {
		it('Should update a book', (done) => {
			const updatedBook = {
				id: 1,
				name: 'updated Book',
			};

			request
				.put(`${path}/${defaultBook.id}`)
				.send(updatedBook)
				.end((err, res) => {
					console.log('UPDATE RESPONSE', res.body);
					expect(res.body).to.be.eql([1]);
					done(err);
				});
		});
	});

	describe('Route DELETE /books/{id}', () => {
		it('Should delete a book', (done) => {
			request
				.delete(`${path}/${defaultBook.id}`)
				.end((err, res) => {
					console.log('DELETE RESPONSE', res.body);
					expect(204);
					done(err);
				});
		});
	});
});
