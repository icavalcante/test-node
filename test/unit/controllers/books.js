import BooksController from '../../../controllers/books'
describe('Controllers: Books', () => {
	describe('Get all Books: getAll()', () => {
		it('should return a list of books', () => {
			const Books = {
				findAll: td.function()
			};

			const expectedResponse = [{
				id:1,
				name:'testBooks',
				created_at:'2017-04-16T21:32:40.027Z',
				updated_at:'2017-04-16T21:32:40.027Z'
			}];

			td.when(Books.findAll({})).thenResolve(expectedResponse);

			const booksController = new BooksController(Books);
			return booksController.getAll()
			.then(response => expect(response.data).to.be.eql(expectedResponse));
		});
	});

	describe('Get all Books: getById()', () => {
		it('should return a books', () => {
			const Books = {
				findOne: td.function()
			};

			const expectedResponse = {
				id:1,
				name:'testBooks',
				created_at:'2017-04-16T21:32:40.027Z',
				updated_at:'2017-04-16T21:32:40.027Z'
			};

			td.when(Books.findOne({where: {id: 1}})).thenResolve(expectedResponse);

			const booksController = new BooksController(Books);
			return booksController.getById({id: 1})
			.then(response => expect(response.data).to.be.eql(expectedResponse));
		});
	});

	describe('Create a book: create()', () => {
		it('should create a books', () => {
			const Books = {
				create: td.function()
			};
			const requestBody = {
				name: 'Test Book'
			};
			const expectedResponse = {
				id:1,
				name:'testBooks',
				created_at:'2017-04-16T21:32:40.027Z',
				updated_at:'2017-04-16T21:32:40.027Z'
			};

			td.when(Books.create(requestBody)).thenResolve(expectedResponse);

			const booksController = new BooksController(Books);
			return booksController.create(requestBody)
			.then(response => {
				expect(response.statusCode).to.be.eql(201);
				expect(response.data).to.be.eql(expectedResponse);
			});
		});
	});
});