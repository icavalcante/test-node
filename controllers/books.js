const defaultResponse = (data,statusCode = 200) => ({
	data,
	statusCode
});

const errorResponse = (message, statusCode = 400) => defaultResponse({
	error: message
}, statusCode);

class BooksController{
	constructor(Books){
		this.Books = Books
	}

	getAll(){
		return this.Books.findAll({})
			.then(result => defaultResponse(result))
			.catch(error => errorResponse(error.message));
			// res.json([{
			// 	id: 1,
			// 	name: 'Default Book'
			// }])
	}

	getById(params){
		return this.Books.findOne({ where: params })
		.then(result => defaultResponse(result))
		.catch(() => errorResponse(error.message));
	}

	create(data){
		return this.Books.create(data)
		.then(result => defaultResponse(result, 201))
		.catch(() => errorResponse(error.message, 422));
	}
}

export default BooksController;